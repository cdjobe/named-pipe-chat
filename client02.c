// Carson Jobe - HW 3 - Operating Systems 
// 2nd chat client
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>


int main()
{

    int server_Pipe, client_Pipe;
    server_Pipe = open ("server_pipe", O_RDWR, O_NONBLOCK);
	client_Pipe = open ("client_pipe", O_RDWR, O_NONBLOCK);

	if (server_Pipe < 0) {
		printf("Error in opening pipe.\n");
		exit(-1);
	}
	if (client_Pipe < 0) {
		printf ("Error in opening client pipe.\n");
		exit(-1);
	}

/*******************************************************************/

    /****VARIABLES****/
   	int message_Size = 256 * sizeof (char);
  	int name_Size = 10 * sizeof (char);

    // Client 01 Details
	char my_Message[message_Size];
    char user_Name[name_Size];
    // Client 02 Details    
    char chatting_With[name_Size];
    char other_Message[message_Size];
    
     
    /****GET/SEND INFO****/
	printf ("\n-----Welcome to Chat-----\n\n\n");
	printf ("Please enter user name: ");
    scanf ("%s", user_Name); 
//	if ((strlen (user_Name) > 0) && (user_Name[name_Size - 1] == '\n')) {
//		user_Name[name_Size - 1] = '\0';
//	}


    write (server_Pipe, user_Name, name_Size);      // send user name
    read (client_Pipe, chatting_With, name_Size);   // get other's name   
    printf ("\nYou are now chatting with, %s\n", chatting_With);
    close(client_Pipe);
    close(server_Pipe);
/*******************************************************************/

    int chat = 1;       // 1 = true, 0 = false
    int first_Run = 1;  // 1 = true, 0 = false
    while (chat != 0)
    {
        // Open pipes
	    server_Pipe = open ("server_pipe", O_RDWR, O_NONBLOCK);
        client_Pipe = open ("client_pipe", O_RDWR, O_NONBLOCK);

	    /****Get/Send user input****/
        fgets(my_Message, message_Size, stdin);
        if ((strlen(my_Message) > 0) && (my_Message[message_Size - 1] == '\n')) {
            my_Message[message_Size - 1] = '\0';
        }
        
        // Check exit condition. Then Exit
        if (strcmp(my_Message, "Exit()\n") == 0) { 
            chat = 0;
            printf("You have exited the chat.\n");
            strcat(my_Message, user_Name);
            strcat(my_Message, " has exited the chat\n");
            strcat(my_Message, "Type 'Exit()' to exit the chat\n");
        }        
        write (server_Pipe, my_Message, message_Size);


        /****Receive/print other's input****/

        read (client_Pipe, other_Message, message_Size);
        if (first_Run != 1) {
            printf ("%s> %s", chatting_With, other_Message);
        }
        first_Run = 0;
        close (client_Pipe); // close client02 pipe for 01 to write
        close (server_Pipe);
    }
	

	close (server_Pipe);
	close (client_Pipe);
	
	return(0);
}