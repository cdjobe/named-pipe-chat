// Carson Jobe - HW 3 - Operating Systems 
// client01.c (acting as server)


#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

void CreatePipes()
// Creates the pipes that will be used for server and client
{
	int file1, file2;
	
	// Create named pipes
	file1 = mkfifo ("server_pipe", 0666);
	file2 = mkfifo ("client_pipe", 0666);
	
	if (file1 < 0 || file2 < 0) {
		printf ("Unable to create server pipe.\n");
//		exit (1);
	}

	printf ("Server and client have been created.\n");
} // End CreatePipes()

void CleanPipes()
// Clean up files
{
	close("server_pipe");
	close("client_pipe");
	remove("server_pipe");
	remove("client_pipe");
} // End CleanPipes()

char *trimString( char *the_String, const char *to_Delete)
// replaces the line end with null.

{
    if (the_String) {
        char *replace_Char;
        while (replace_Char = strpbrk(the_String, to_Delete)) {
            *replace_Char = 0;
        }
    }
    
    return the_String;
} // End trimString

int main()
{
    remove("server_pipe");
    remove("client_pipe");
	CreatePipes();

    int server_Pipe, client_Pipe;
	server_Pipe = open ("server_pipe", O_RDWR, O_NONBLOCK);
	client_Pipe = open ("client_pipe", O_RDWR, O_NONBLOCK);

	if (server_Pipe < 1 || client_Pipe < 1) {
		printf("Error opening pipe\n");
	}

/*******************************************************************/

    /****VARIABLES****/
   	int message_Size = 256 * sizeof (char);
  	int name_Size = 10 * sizeof (char);

    // Client 01 Details
	char my_Message[message_Size];
    char user_Name[name_Size];
    // Client 02 Details    
    char chatting_With[name_Size];
    char other_Message[message_Size];


    /****GET/SEND INFO****/
    printf ("\n-----Welcome to Chat-----\n--Type 'Exit()' to quit.--\n\n");
	printf ("Please enter user name: ");
	scanf("%s", user_Name);

	write (client_Pipe, user_Name, name_Size);      // Send user name
    read (server_Pipe, chatting_With, name_Size);   // Get other's name
    printf ("\nYou are now chatting with %s\n", chatting_With);
    close(server_Pipe);
    close(client_Pipe);
/*******************************************************************/

	int chat = 1;
    int first_Run = 1; // 1 = true, 0 = false. 
	while (chat != 0)
	{
        /****Receive/print other's input****/
        server_Pipe = open ("server_pipe", O_RDWR, O_NONBLOCK);
        client_Pipe = read (server_Pipe, &other_Message, message_Size);
        if (first_Run != 1) {
            printf ("%s> %s", chatting_With, &other_Message);
        }
        first_Run = 0;
        close (server_Pipe);
        
	    /****Get/Send user input****/
	    client_Pipe = open ("client_pipe", O_RDWR, O_NONBLOCK);
        fgets(my_Message, message_Size, stdin);
        if ((strlen(my_Message) > 0) && (my_Message[message_Size - 1] == '\n')) {
            my_Message[message_Size - 1] = '\0';
        }

        // Check exit condition. Then Exit
        if (strcmp(my_Message, "Exit()\n") == 0) { 
            chat = 0;
            printf("You have exited the chat.\n");
            strcat(my_Message, user_Name);
            strcat(my_Message, " has exited the chat\n");
            strcat(my_Message, "Type 'Exit()' to exit the chat\n");
        }

        write (client_Pipe, my_Message, message_Size);
        close (client_Pipe);

	}

	CleanPipes();
	return(0);
}
		